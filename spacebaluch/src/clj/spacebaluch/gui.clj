(ns spacebaluch.gui
  (:require
    [clojure.core.async :refer [go <! put! chan]]
    [spacebaluch.perlin :as perlin])
  (:import
    (javax.swing JFrame JPanel JLabel)
    (java.awt Color Dimension Graphics Rectangle)
    (java.awt.color ColorSpace)
    (java.awt.image BufferedImage)))

(defn hsv
  [h s v]
  (Color/getHSBColor h s v))

(defonce gui (atom nil))

(defn perlin-panel
  [[w h :as dims]]
  (let [res 2
        frame-data (atom (doall (for [x (range w)
                                      y (range h)]
                                  [x y (hsv (rand) 1 1)])))
        img (atom (BufferedImage. w h BufferedImage/TYPE_INT_RGB))
        canvas (proxy [JLabel] []
                 (paint [g] (.drawImage g @img 0 0 this)))
        paint-canvas (fn []
                       (let [new-img (BufferedImage. w h BufferedImage/TYPE_INT_RGB)
                             g (.createGraphics new-img)]
                         (doseq [[x y c] @frame-data]
                           (.setColor g c)
                           (.fill g (Rectangle. (* x res) (* y res) res res)))
                         (reset! img new-img)
                         (.repaint canvas)))]
    (paint-canvas)
    (go (let [pgf (perlin/noise-generator {:dimensions 3
                                           :octaves 3
                                           :tile [100 100 4]})]
          (loop [frame 0]
            (->> (for [x (range w)
                       y (range h)
                       :let [n (pgf [(/ x res) (/ y res) frame])
                             a (/ (inc n) 2)]]
                   [x y (hsv a 1 1)])
                 (reset! frame-data))
            doall
            (println "looped")
            (paint-canvas)
            (println "drew")
            (when-not (nil? @gui)
              (recur (inc frame))))))
    canvas))

(defn show-gui
  []
  (reset!
    gui
    (doto (JFrame. "Perlin")
      (.setDefaultCloseOperation JFrame/DISPOSE_ON_CLOSE)
      (.setSize 800 800)
      (.add (perlin-panel [600 600]))
      (.pack)
      (.setVisible true))))

(defn stop!
  []
  (.hide @gui)
  (reset! gui nil))
