(ns spacebaluch.core
  (:require-macros
    [cljs.core.async.macros :refer [go]]
    [servant.macros :refer [defservantfn]])
  (:require
    [cognitect.transit :as t]
    [reagent.core :as r]
    [reagent.session :as session]
    [servant.core :as servant]
    [servant.worker :as worker]
    [spacebaluch.perlin :as perlin])
  (:import
    (goog.events KeyCodes)))

(defn div [x y]
 (js/Math.floor (/ x y)))

;; Transit stuff

(defn clj->transit
  [x]
  (t/write (t/writer :json) x))

(defn transit->clj
  [transit]
  (t/read (t/reader :json) transit))

;; Web worker stuff

(def worker-count 1)

(def worker-script "/js/app.js")

(defservantfn compute-frame
  [width height frame res]
  (let [pgf (perlin/noise-generator {:dimensions 2
                                     :octaves 3
                                     :tile [10 10 4]})]
    (-> (for [x (range width)
              y (range height)
              :let [n (pgf [(/ x res) (/ y res) (/ frame 2)])
                    a (/ (inc n) 2)]]
          [x y (str "rgba(7,224,226," a ")")])
        doall
        clj->transit)))

;; -------------------------
;; Views

(defn home-page []
  (let [canvas-dims (r/atom {:width 1024 :height 900})
        keys-pressed (r/atom #{})
        code->sym {KeyCodes/UP :up
                   KeyCodes/DOWN :down
                   KeyCodes/LEFT :left
                   KeyCodes/RIGHT :right}
        baluch-pos (r/atom {:x 0 :y 0})
        handle-key-presses
        (fn handle-key-presses []
          (when-let [pressed (seq @keys-pressed)]
            (doseq [dir pressed]
              (case dir
                :up (swap! baluch-pos update :y + 5)
                :down (swap! baluch-pos update :y - 5)
                :left (swap! baluch-pos update :x - 5)
                :right (swap! baluch-pos update :x + 5))))
          (.requestAnimationFrame js/window handle-key-presses))
        draw-frame (fn [c res frame]
                     (let [ctx (.getContext c "2d")
                           {:keys [width height]} @canvas-dims]
                       (.clearRect ctx 0 0 width height)
                       (doseq [[x y color] frame]
                         (set! (.-fillStyle ctx) color)
                         (.fillRect ctx (* x res) (* y res) res res))))
        started? (r/atom false)
        start-drawing
        (fn [c]
          (let [res 2
                spawn (fn [f]
                        (let [{:keys [width height]} @canvas-dims]
                          (servant/servant-thread
                            servant-channel
                            servant/standard-message
                            compute-frame (div width 10) (div height 10) f res)))]
            (go
              (loop [draw-chan (spawn 0)
                     frame-count 1]
                (let [frame-transit (<! draw-chan)
                      frame (transit->clj frame-transit)]
                  (draw-frame c 10 frame)
                  (recur (spawn frame-count) (inc frame-count)))))))]
    (r/create-class
      {:display-name "canvas thing"

       :component-did-mount
       (fn [_]
         (js/setTimeout
           (fn [] (reset! canvas-dims
                          {:width (.. js/document -body -clientWidth)
                           :height (.. js/document -body -clientHeight)}))
           0)
         (.addEventListener
           js/document "keydown"
           (fn [e]
             (when-let [sym (code->sym (.-keyCode e))]
               (.preventDefault e)
               (swap! keys-pressed conj sym))))
         (.addEventListener
           js/document "keyup"
           (fn [e]
             (when-let [sym (code->sym (.-keyCode e))]
               (.preventDefault e)
               (swap! keys-pressed disj sym))))
         (.requestAnimationFrame js/window handle-key-presses))

       :reagent-render
       (fn []
         [:div
          [:img
           {:style {:position "absolute"
                    :left (:x @baluch-pos)
                    :bottom (:y @baluch-pos)}
            :src "baluch.gif"}]
          ^{:key "drawingcanvas"}
          [:canvas
           (merge {:ref (fn [node]
                          (when (and node (some? @canvas-dims) (not @started?))
                            (reset! started? true)
                            (start-drawing node)))}
                  @canvas-dims)]
          [:h2.greeting
           "Happy Birthday Sadie!"]])})))

;; -------------------------
;; Initialize app

(defn mount-root []
  (r/render [home-page] (.getElementById js/document "app")))

(defn init! []

  (enable-console-print!)

  (def servant-channel (servant/spawn-servants worker-count worker-script))
  (mount-root))

(if (servant/webworker?)
  (worker/bootstrap)
  (init!))
